Basic IP Configuration
-------------------------------------------
Thu Dec 29 19:43:01 2022   Br3   Router>enable 
Thu Dec 29 19:43:05 2022   Br3   Router#conf Terminal 
Thu Dec 29 19:43:10 2022   Br3   Router(config)#hostname BR3
Thu Dec 29 19:43:15 2022   Br3   BR3(config)#int f0/1
Thu Dec 29 19:43:33 2022   Br3   BR3(config-if)#ip add 172.16.3.1 255.255.255.128
Thu Dec 29 19:43:35 2022   Br3   BR3(config-if)#no sh
Thu Dec 29 19:43:43 2022   Br3   BR3(config-if)#description Sales LAN
Thu Dec 29 19:43:44 2022   Br3   BR3(config-if)#exit
Thu Dec 29 19:44:09 2022   Br3   BR3(config)#int f0/0
Thu Dec 29 19:44:31 2022   Br3   BR3(config-if)#ip add 172.16.1.1 255.255.255.224
Thu Dec 29 19:44:32 2022   Br3   BR3(config-if)#no sh
Thu Dec 29 19:44:38 2022   Br3   BR3(config-if)#description Broadcast Network
Thu Dec 29 19:44:40 2022   Br3   BR3(config-if)#exit
Thu Dec 29 19:45:03 2022   BR4   Router>en
Thu Dec 29 19:45:04 2022   BR4   Router#conf t
Thu Dec 29 19:45:08 2022   BR4   Router(config)#hostname BR4
Thu Dec 29 19:45:19 2022   BR4   BR4(config)#int f0/1
Thu Dec 29 19:45:27 2022   BR4   BR4(config-if)#ip add 172.16.2.1 255.255.255.128
Thu Dec 29 19:45:28 2022   BR4   BR4(config-if)#no sh
Thu Dec 29 19:45:33 2022   BR4   BR4(config-if)#description Marketing LAN
Thu Dec 29 19:45:33 2022   BR4   BR4(config-if)#ex
Thu Dec 29 19:45:36 2022   BR4   BR4(config)#int f0/0
Thu Dec 29 19:45:51 2022   BR4   BR4(config-if)#ip add 172.16.1.2 255.255.255.224
Thu Dec 29 19:45:52 2022   BR4   BR4(config-if)#no sh
Thu Dec 29 19:46:01 2022   BR4   BR4(config-if)#description Broadcast Network
Thu Dec 29 19:46:01 2022   BR4   BR4(config-if)#exit
Thu Dec 29 19:46:49 2022   BR2   Router>en
Thu Dec 29 19:46:50 2022   BR2   Router#conf t
Thu Dec 29 19:46:53 2022   BR2   Router(config)#hostname BR2
Thu Dec 29 19:46:56 2022   BR2   BR2(config)#int f0/0
Thu Dec 29 19:47:03 2022   BR2   BR2(config-if)#ip add 172.16.1.3 255.255.255.224
Thu Dec 29 19:47:05 2022   BR2   BR2(config-if)#no sh
Thu Dec 29 19:47:11 2022   BR2   BR2(config-if)#description Broadcast Network
Thu Dec 29 19:47:13 2022   BR2   BR2(config-if)#exit
Thu Dec 29 19:47:19 2022   BR2   BR2(config)#int s0/1/0
Thu Dec 29 19:47:34 2022   BR2   BR2(config-if)#ip add 172.16.10.1 255.255.255.252
Thu Dec 29 19:47:37 2022   BR2   BR2(config-if)#no sh
Thu Dec 29 19:47:43 2022   BR2   BR2(config-if)#description Link to BR1
Thu Dec 29 19:47:45 2022   BR2   BR2(config-if)#exit
Thu Dec 29 19:47:57 2022   BR1   Router>en
Thu Dec 29 19:47:58 2022   BR1   Router#conf t
Thu Dec 29 19:48:01 2022   BR1   Router(config)#hostname BR1
Thu Dec 29 19:48:07 2022   BR1   BR1(config)#int s0/1/0
Thu Dec 29 19:48:16 2022   BR1   BR1(config-if)#ip add 172.16.10.2 255.255.255.252
Thu Dec 29 19:48:17 2022   BR1   BR1(config-if)#no sh
Thu Dec 29 19:48:21 2022   BR1   BR1(config-if)#description Link to BR2
Thu Dec 29 19:48:22 2022   BR1   BR1(config-if)#exit
Thu Dec 29 19:48:26 2022   BR1   BR1(config)#int f0/0
Thu Dec 29 19:48:37 2022   BR1   BR1(config-if)#ip add 172.16.4.1 255.255.255.192
Thu Dec 29 19:48:38 2022   BR1   BR1(config-if)#no sh
Thu Dec 29 19:48:44 2022   BR1   BR1(config-if)#description Data Center
Thu Dec 29 19:48:45 2022   BR1   BR1(config-if)#exit
Thu Dec 29 19:48:51 2022   BR1   BR1(config)#int f0/1
Thu Dec 29 19:49:08 2022   BR1   BR1(config-if)#ip add 11.11.11.2 255.255.255.252
Thu Dec 29 19:49:09 2022   BR1   BR1(config-if)#no sh
Thu Dec 29 19:49:16 2022   BR1   BR1(config-if)#description Link to ISP
Thu Dec 29 19:49:17 2022   BR1   BR1(config-if)#exit
Thu Dec 29 19:50:27 2022   ISP   Router>en
Thu Dec 29 19:50:29 2022   ISP   Router#conf t
Thu Dec 29 19:50:31 2022   ISP   Router(config)#hostname ISP
Thu Dec 29 19:50:34 2022   ISP   ISP(config)#int f0/0
Thu Dec 29 19:50:43 2022   ISP   ISP(config-if)#ip add 11.11.11.1 255.255.255.252
Thu Dec 29 19:50:44 2022   ISP   ISP(config-if)#no sh
Thu Dec 29 19:50:49 2022   ISP   ISP(config-if)#description Link to BR1
Thu Dec 29 19:50:51 2022   ISP   ISP(config-if)#exit
Thu Dec 29 19:50:54 2022   ISP   ISP(config)#int f0/1
Thu Dec 29 19:51:05 2022   ISP   ISP(config-if)#ip add 8.8.8.9 255.0.0.0
Thu Dec 29 19:51:07 2022   ISP   ISP(config-if)#no sh
Thu Dec 29 19:51:08 2022   ISP   ISP(config-if)#exit

OSPF
====================================

Thu Dec 29 19:52:59 2022   Br3   BR3(config)#do show ip route connected
Thu Dec 29 19:53:14 2022   Br3   BR3(config)#router ospf 114
Thu Dec 29 19:53:42 2022   Br3   BR3(config-router)#network 172.16.1.0 0.0.0.31 area 0
Thu Dec 29 19:53:55 2022   Br3   BR3(config-router)#network 172.16.3.0 0.0.0.127 area 0
Thu Dec 29 19:54:07 2022   Br3   BR3(config-router)#passive-interface f0/1
Thu Dec 29 19:54:25 2022   Br3   BR3(config-router)#router-id 3.3.3.3
Thu Dec 29 19:54:31 2022   Br3   BR3(config-router)#end

Thu Dec 29 19:54:35 2022   Br3   BR3#reload

Thu Dec 29 19:54:52 2022   BR4   BR4(config)#do sh ip route connected
Thu Dec 29 19:54:58 2022   BR4   BR4(config)#router ospf 114
Thu Dec 29 19:55:08 2022   BR4   BR4(config-router)#network 172.16.1.0 0.0.0.31 area 0
Thu Dec 29 19:55:15 2022   BR4   BR4(config-router)#network 172.16.2.0 0.0.0.127 area 0
Thu Dec 29 19:55:19 2022   BR4   BR4(config-router)#passive-interface f0/1
Thu Dec 29 19:55:24 2022   BR4   BR4(config-router)#router-id 4.4.4.4
Thu Dec 29 19:55:26 2022   BR4   BR4(config-router)#end

Thu Dec 29 19:55:29 2022   BR4   BR4#reload

Thu Dec 29 19:55:46 2022   BR2   BR2(config)#do sh ip route connected
Thu Dec 29 19:55:52 2022   BR2   BR2(config)#router ospf 114
Thu Dec 29 19:56:02 2022   BR2   BR2(config-router)#network 172.16.1.0 0.0.0.31 area 0
Thu Dec 29 19:56:27 2022   BR2   BR2(config-router)#network 172.16.10.0 0.0.0.3 area 1
Thu Dec 29 19:56:37 2022   BR2   BR2(config-router)#router-id 2.2.2.2
Thu Dec 29 19:56:39 2022   BR2   BR2(config-router)#end

Thu Dec 29 19:56:42 2022   BR2   BR2#reload

Thu Dec 29 19:57:09 2022   BR1   BR1(config)#do sh ip route connected
Thu Dec 29 19:57:31 2022   BR1   BR1(config)#router ospf 114
Thu Dec 29 19:57:53 2022   BR1   BR1(config-router)#network 172.16.4.0 0.0.0.63 area 1
Thu Dec 29 19:58:01 2022   BR1   BR1(config-router)#network 172.16.10.0 0.0.0.3 area 1
Thu Dec 29 19:58:06 2022   BR1   BR1(config-router)#passive-interface f0/0
Thu Dec 29 19:58:11 2022   BR1   BR1(config-router)#router-id 1.1.1.1
Thu Dec 29 19:58:12 2022   BR1   BR1(config-router)#end

Thu Dec 29 19:58:15 2022   BR1   BR1#reload


Static Route
-------------------------------
Thu Dec 29 19:59:21 2022   BR1   BR1(config)#ip route 0.0.0.0 0.0.0.0 11.11.11.1
Thu Dec 29 20:02:14 2022   ISP   ISP(config)#ip route 172.16.0.0 255.255.240.0 11.11.11.2

To forward the default routing configuration information to all routers in the ospf process------
===========================================================================
Thu Dec 29 20:03:42 2022   BR1   BR1(config)#router ospf 114
Thu Dec 29 20:03:51 2022   BR1   BR1(config-router)#default-information originate 
Thu Dec 29 20:03:54 2022   BR1   BR1(config-router)#exit

BLOCK Ptivate IP to the Internet
----------------------------------------
Thu Dec 29 20:05:48 2022   ISP   ISP(config)#ip access-list standard BLOCK-PRIVATE
Thu Dec 29 20:06:00 2022   ISP   ISP(config-std-nacl)#deny 10.0.0.0 0.255.255.255
Thu Dec 29 20:06:07 2022   ISP   ISP(config-std-nacl)#deny 172.16.0.0 0.15.255.255
Thu Dec 29 20:06:13 2022   ISP   ISP(config-std-nacl)#deny 192.168.0.0 0.0.255.255
Thu Dec 29 20:06:17 2022   ISP   ISP(config-std-nacl)#permit any
Thu Dec 29 20:06:20 2022   ISP   ISP(config-std-nacl)#exit

Thu Dec 29 20:06:32 2022   ISP   ISP(config)#int f0/0
Thu Dec 29 20:06:40 2022   ISP   ISP(config-if)#ip access-group BLOCK-PRIVATE in
Thu Dec 29 20:06:41 2022   ISP   ISP(config-if)#exit

PAT Configure
----------------------------------------------
Thu Dec 29 20:08:03 2022   BR1   BR1(config)#ip access-list standard PAT-LIST
Thu Dec 29 20:08:40 2022   BR1   BR1(config-std-nacl)#permit 172.16.0.0 0.0.15.255
Thu Dec 29 20:08:42 2022   BR1   BR1(config-std-nacl)#deny any
Thu Dec 29 20:08:43 2022   BR1   BR1(config-std-nacl)#exit

Thu Dec 29 20:09:25 2022   BR1   BR1(config)#ip nat inside source list PAT-LIST interface  f0/1 overload 

Thu Dec 29 20:09:40 2022   BR1   BR1(config)#int s0/1/0
Thu Dec 29 20:09:53 2022   BR1   BR1(config-if)#ip nat in
Thu Dec 29 20:09:54 2022   BR1   BR1(config-if)#exit

Thu Dec 29 20:10:02 2022   BR1   BR1(config)#int f0/0
Thu Dec 29 20:10:05 2022   BR1   BR1(config-if)#ip nat in
Thu Dec 29 20:10:06 2022   BR1   BR1(config-if)#exit

Thu Dec 29 20:10:12 2022   BR1   BR1(config)#int f0/1
Thu Dec 29 20:10:16 2022   BR1   BR1(config-if)#ip nat out
Thu Dec 29 20:10:18 2022   BR1   BR1(config-if)#exit

Extended ACL-Sales team cannot browse www.ice.edu
-------------------------------------------------------
Thu Dec 29 20:11:52 2022   Br3   BR3(config)#ip access-list extended BLOCK-BROWSE
Thu Dec 29 20:14:23 2022   Br3   BR3(config-ext-nacl)#deny tcp 172.16.3.0 0.0.0.127 172.16.4.2 0.0.0.0 eq www
Thu Dec 29 20:14:33 2022   Br3   BR3(config-ext-nacl)#permit ip any any
Thu Dec 29 20:14:35 2022   Br3   BR3(config-ext-nacl)#exit

Thu Dec 29 20:15:00 2022   Br3   BR3(config)#int f0/1
Thu Dec 29 20:15:11 2022   Br3   BR3(config-if)#ip access-group BLOCK-BROWSE in
Thu Dec 29 20:15:12 2022   Br3   BR3(config-if)#exit

NTP & SYSLOG & BackUP
-----------------------------
Thu Dec 29 20:16:30 2022   Br3   BR3(config)#ntp server 172.16.4.3
Thu Dec 29 20:16:33 2022   Br3   BR3(config)#ntp update-calendar
Thu Dec 29 20:16:57 2022   Br3   BR3(config)#logging 172.16.4.3
Thu Dec 29 20:17:17 2022   Br3   BR3(config)#service timestamps log datetime msec 
Thu Dec 29 20:17:19 2022   Br3   BR3(config)#exit
Thu Dec 29 20:18:14 2022   Br3   BR3#copy running-config tftp:
                                 Address or name of remote host []? 172.16.4.3
                                 Destination filename [BR1-confg]?

Thu Dec 29 20:17:35 2022   BR4   BR4(config)#ntp server 172.16.4.3
Thu Dec 29 20:17:41 2022   BR4   BR4(config)#ntp update-calendar 
Thu Dec 29 20:17:45 2022   BR4   BR4(config)#logging 172.16.4.3
Thu Dec 29 20:17:52 2022   BR4   BR4(config)#service timestamps log datetime msec 
Thu Dec 29 20:18:38 2022   BR4   BR4#copy running-config tftp: 
                                 Address or name of remote host []? 172.16.4.3
                                 Destination filename [BR1-confg]?


Thu Dec 29 20:18:58 2022   BR2   BR2(config)#ntp server 172.16.4.3
Thu Dec 29 20:19:01 2022   BR2   BR2(config)#ntp update-calendar 
Thu Dec 29 20:19:05 2022   BR2   BR2(config)#logging 172.16.4.3
Thu Dec 29 20:19:09 2022   BR2   BR2(config)#service timestamps log datetime msec 
Thu Dec 29 20:19:10 2022   BR2   BR2(config)#ex
Thu Dec 29 20:19:14 2022   BR2   BR2#copy running-config tftp: 
                                 Address or name of remote host []? 172.16.4.3
                                 Destination filename [BR1-confg]?

Thu Dec 29 20:19:36 2022   BR1   BR1(config)#ntp server 172.16.4.3
Thu Dec 29 20:19:40 2022   BR1   BR1(config)#ntp update-calendar 
Thu Dec 29 20:19:44 2022   BR1   BR1(config)#logging 172.16.4.3
Thu Dec 29 20:19:48 2022   BR1   BR1(config)#service timestamps log datetime msec 
Thu Dec 29 20:19:49 2022   BR1   BR1(config)#end
Thu Dec 29 20:20:11 2022   BR1   BR1#copy running-config tftp: 
                                 Address or name of remote host []? 172.16.4.3
                                 Destination filename [BR1-confg]?


