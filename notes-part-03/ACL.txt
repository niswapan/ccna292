OSPF
-------------------------
Thu Dec 8 21:03:07 2022   Br1   BR1(config)#do sh ip route con
Thu Dec 8 21:03:11 2022   Br1   BR1(config)#router ospf 11
Thu Dec 8 21:03:43 2022   Br1   BR1(config-router)#network 172.16.5.0 0.0.0.255 area 0
Thu Dec 8 21:03:52 2022   Br1   BR1(config-router)#network 172.16.10.0 0.0.0.127 area 0
Thu Dec 8 21:04:00 2022   Br1   BR1(config-router)#network 172.16.20.0 0.0.0.3 area 0
Thu Dec 8 21:04:06 2022   Br1   BR1(config-router)#passive-interface f0/0
Thu Dec 8 21:04:07 2022   Br1   BR1(config-router)#passive-interface f0/1
Thu Dec 8 21:04:08 2022   Br1   BR1(config-router)#exit

Thu Dec 8 21:04:19 2022   Br2   BR2(config)#do sh ip route con
Thu Dec 8 21:04:24 2022   Br2   BR2(config)#router ospf 11
Thu Dec 8 21:04:33 2022   Br2   BR2(config-router)#network 172.16.20.0 0.0.0.3 area 0
Thu Dec 8 21:04:46 2022   Br2   BR2(config-router)#network 192.168.5.0 0.0.0.255 area 0
Thu Dec 8 21:04:59 2022   Br2   BR2(config-router)#network 192.168.10.0 0.0.0.63 area 0
Thu Dec 8 21:05:02 2022   Br2   BR2(config-router)#passive-interface f0/0
Thu Dec 8 21:05:04 2022   Br2   BR2(config-router)#passive-interface f0/1
Thu Dec 8 21:05:05 2022   Br2   BR2(config-router)#exit

Standard ACL [Numbered]
--------------------------------------- 
Thu Dec 8 21:14:37 2022   Br2   BR2(config)#access-list 10 deny host 172.16.5.3
Thu Dec 8 21:24:07 2022   Br2   BR2(config)#access-list 10 permit any

Thu Dec 8 21:17:25 2022   Br2   BR2(config)#interface f0/0
Thu Dec 8 21:17:42 2022   Br2   BR2(config-if)#ip access-group 10 out 
Thu Dec 8 21:23:53 2022   Br2   BR2(config-if)#ex

View Access-lists
------------------------------------
Thu Dec 8 21:24:17 2022   Br2   BR2#show access-lists 

Thu Dec 8 21:40:25 2022   Br2   BR2(config)#ip access-list extended Block_Web
Thu Dec 8 21:44:46 2022   Br2   BR2(config-ext-nacl)#deny  tcp  host 192.168.5.2 host 172.16.10.2 eq 80
Thu Dec 8 21:46:11 2022   Br2   BR2(config-ext-nacl)#permit ip any any
Thu Dec 8 21:47:00 2022   Br2   BR2(config-ext-nacl)#exit

Thu Dec 8 21:49:48 2022   Br2   BR2(config)#int f0/0
Thu Dec 8 21:50:05 2022   Br2   BR2(config-if)#ip access-group Block_Web in
Thu Dec 8 21:50:06 2022   Br2   BR2(config-if)#exit

