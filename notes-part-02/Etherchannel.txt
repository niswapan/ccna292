PAgP
----------------------------
Sun Nov 6 20:50:24 2022   Switch2   Switch(config)#interface range f0/1, f0/3
Sun Nov 6 20:52:36 2022   Switch2   Switch(config-if-range)#channel-group 1 mode desirable 
Sun Nov 6 20:52:41 2022   Switch2   Switch(config-if-range)#exit

Sun Nov 6 20:56:31 2022   Switch2   Switch(config)#interface port-channel 1
Sun Nov 6 20:56:34 2022   Switch2   Switch(config-if)#switchport mode trunk 
Sun Nov 6 20:56:37 2022   Switch2   Switch(config-if)#exit

Sun Nov 6 20:53:07 2022   Switch3   Switch(config)#int ran f0/1, f0/3
Sun Nov 6 20:53:35 2022   Switch3   Switch(config-if-range)#channel-group 1 mode desirable 
Sun Nov 6 20:53:38 2022   Switch3   Switch(config-if-range)#exit

Sun Nov 6 20:57:51 2022   Switch3   Switch(config)#int port-channel 1
Sun Nov 6 20:57:53 2022   Switch3   Switch(config-if)#switchport mode trunk 
Sun Nov 6 20:57:54 2022   Switch3   Switch(config-if)#exit

LACP
-------------------------------
Sun Nov 6 20:58:42 2022   Switch2   Switch(config)#int ran f0/2, f0/4
Sun Nov 6 20:59:11 2022   Switch2   Switch(config-if-range)#channel-group 2 mode active 
Sun Nov 6 20:59:13 2022   Switch2   Switch(config-if-range)#exit

Sun Nov 6 20:59:22 2022   Switch2   Switch(config)#int port-channel 2
Sun Nov 6 20:59:24 2022   Switch2   Switch(config-if)#switchport mode trunk 
Sun Nov 6 20:59:26 2022   Switch2   Switch(config-if)#exit

Sun Nov 6 20:59:51 2022   Switch4   Switch(config)#int ran f0/1, f0/3
Sun Nov 6 21:00:03 2022   Switch4   Switch(config-if-range)#channel-group 2 mode passive
Sun Nov 6 21:00:07 2022   Switch4   Switch(config-if-range)#exit

Sun Nov 6 21:00:12 2022   Switch4   Switch(config)#int port-channel 2
Sun Nov 6 21:00:14 2022   Switch4   Switch(config-if)#switchport mode trunk 
Sun Nov 6 21:00:16 2022   Switch4   Switch(config-if)#exit
Sun Nov 6 21:00:29 2022   Switch2   Switch(config)#ex

View Etherchannel
===================================
Sun Nov 6 21:00:37 2022   Switch2   Switch#show etherchannel port-channel 
Sun Nov 6 21:01:41 2022   Switch2   Switch#show etherchannel summary 

